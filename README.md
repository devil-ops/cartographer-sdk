[![pipeline status](https://gitlab.oit.duke.edu/devil-ops/cartographer-sdk/badges/main/pipeline.svg)](https://gitlab.oit.duke.edu/devil-ops/cartographer-sdk/-/commits/main) 
[![coverage report](https://gitlab.oit.duke.edu/devil-ops/cartographer-sdk/badges/main/coverage.svg)](https://gitlab.oit.duke.edu/devil-ops/cartographer-sdk/-/commits/main) 

# Cartographer SDK

To get started, just set these env vars:

```
export CARTOGRAPHER_USER=your_user
export CARTOGRAPHER_KEY=your_api_key
```

### Disclaimer

*This code is freely available for non-commercial use and is provided as-is with no warranty.*