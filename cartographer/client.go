package cartographer

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

const (
	baseURL = "https://cartographer.oit.duke.edu"
)

type Client struct {
	client    *http.Client
	UserAgent string
	Config    ClientConfig
	BaseURL   string
	VRF       VRFService
	Device    DeviceService
	IP        IPService
	// Location  LocationService
	// Volume    VolumeService
}

type ClientConfig struct {
	APIUser        string
	APIKey         string
	APIURL         string
	LegacyVRFNames bool
}
type Response struct {
	*http.Response
}

func (cfg *ClientConfig) MakeDefaults() {
	if cfg.APIUser == "" {
		envUser := os.Getenv("CARTOGRAPHER_USER")
		if envUser == "" {
			log.Fatal("Must set the APIUser or use CARTOGRAPHER_USER")
		} else {
			cfg.APIUser = envUser
		}
	}
	if cfg.APIKey == "" {
		// Sort of gross, but use either CARTOGRAPHER_KEY or CARTOGRAPHER_TOKEN
		// Eventually just make this CARTOGRAPHER_TOKEN to line up with the other tools naming convention
		envKey := os.Getenv("CARTOGRAPHER_KEY")
		envToken := os.Getenv("CARTOGRAPHER_TOKEN")
		if envKey == "" && envToken == "" {
			log.Fatal("Must set the APIKey or use CARTOGRAPHER_KEY or CARTOGRAPHER_TOKEN")
		} else {
			if envKey != "" {
				cfg.APIKey = envKey
			} else {
				cfg.APIKey = envToken
			}
		}
	}
	if cfg.APIURL == "" {
		url := os.Getenv("CARTOGRAPHER_URL")
		if url != "" {
			cfg.APIURL = url
		} else {
			cfg.APIURL = baseURL
		}
	}

	lvn := os.Getenv("CARTOGRAPHER_LEGACY_VRF_NAMES")
	if lvn != "" {
		log.Warn("Using legacy VRF names since CARTOGRAPHER_LEGACY_VRF_NAMES is set. This will be removed in a future release, so please start using the real names when you can")
		cfg.LegacyVRFNames = true
	}
}

// NewClient Generic new client creation
func NewClient(config ClientConfig, httpClient *http.Client) *Client {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}

	userAgent := "cartographer-sdk"
	c := &Client{Config: config, client: httpClient, UserAgent: userAgent, BaseURL: config.APIURL}
	// c.getToken(config.LongLivedToken)

	// c.Location = &LocationServiceOp{client: c}
	// c.Volume = &VolumeServiceOp{client: c}
	c.VRF = &VRFServiceOp{client: c}
	c.Device = &DeviceServiceOp{client: c}
	c.IP = &IPServiceOp{client: c}
	return c
}

func NewClientWithEnv(httpClient *http.Client) (*Client, error) {
	user := os.Getenv("CARTOGRAPHER_USER")
	key := os.Getenv("CARTOGRAPHER_KEY")
	if user == "" {
		return nil, errors.New("must set CARTOGRAPHER_USER")
	}
	if key == "" {
		return nil, errors.New("must set CARTOGRAPHER_KEY")
	}
	return NewClient(ClientConfig{
		APIUser: user,
		APIKey:  key,
	}, httpClient), nil
}

func NewClientWithViper(httpClient *http.Client, v *viper.Viper) (*Client, error) {
	user := v.GetString("cartographer-user")
	if user == "" {
		return nil, errors.New("must set cartographer-user inside viper")
	}
	key := v.GetString("cartographer-key")
	if key == "" {
		key = v.GetString("cartographer-token")
		if key == "" {
			return nil, errors.New("must set cartographer-key (or cartographer-token) inside viper")
		}

	}
	host := v.GetString("cartographer-host")
	if host == "" {
		host = "https://cartographer.oit.duke.edu"
	}
	return NewClient(ClientConfig{
		APIUser: user,
		APIKey:  key,
		APIURL:  host,
	}, httpClient), nil
}

func (c *Client) sendRequest(req *http.Request, v interface{}) (*Response, error) {
	// req.Header.Set("Content-Type", "application/json; charset=utf-8")
	// req.Header.Set("Accept", "application/json; charset=utf-8")
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "application/json; charset=utf-8")
	req.SetBasicAuth(c.Config.APIUser, c.Config.APIKey)

	// command, _ := http2curl.GetCurlCommand(req)
	// log.Debugln(command)

	res, err := c.client.Do(req)
	req.Close = true
	if err != nil {
		log.Warn(err)
		return nil, err
	}
	// b, _ := ioutil.ReadAll(res.Body)

	defer res.Body.Close()

	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		var errRes ErrorResponse
		b, _ := ioutil.ReadAll(res.Body)
		log.Warning("Trying to parse error, not sure if this will work")
		log.Warning(string(b))
		if err = json.NewDecoder(res.Body).Decode(&errRes); err == nil {
			return nil, errors.New(errRes.Message)
		}

		if res.StatusCode == http.StatusTooManyRequests {
			return nil, fmt.Errorf("too many requests.  Check rate limit and make sure the userAgent is set right")
		} else if res.StatusCode == http.StatusNotFound {
			return nil, fmt.Errorf("that entry was not found, are you sure it exists?")
		} else {
			return nil, fmt.Errorf("error, status code: %d", res.StatusCode)
		}
	}

	b, _ := ioutil.ReadAll(res.Body)
	if string(b) != "" {
		if err = json.NewDecoder(bytes.NewReader(b)).Decode(&v); err != nil {
			return nil, err
		}
	} else {
		// When there is no body
		return nil, nil
	}
	r := &Response{res}

	return r, nil
}
