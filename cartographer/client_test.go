package cartographer

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/require"
)

var (
	srv *httptest.Server
	c   *Client
)

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	shutdown()
	os.Exit(code)
}

func shutdown() {
	srv.Close()
}

func setup() {
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	log.Logger = log.With().Caller().Logger().Output(zerolog.ConsoleWriter{Out: os.Stderr})
	srv = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch {
		case strings.HasSuffix(r.URL.Path, "/api/v1/vrfs"):
			copyResp("./test_data/vrfs/list.json", w)
		case strings.HasSuffix(r.URL.Path, "/api/v1/vrfs/vrf-foo"):
			copyResp("./test_data/vrfs/vrf-foo.json", w)
		case strings.HasSuffix(r.URL.Path, "/api/v1/vrfs/vrf-bar"):
			copyResp("./test_data/vrfs/vrf-bar.json", w)
		default:
			log.Warn().Str("url", r.URL.String()).Msg("unexpected request")
			w.WriteHeader(http.StatusNotFound)
		}
	}))
	// c = NewClient(conf, nil)
	v := viper.New()
	v.Set("cartographer-user", "foo")
	v.Set("cartographer-key", "bar")
	v.Set("cartographer-host", srv.URL)
	var err error
	c, err = NewClientWithViper(nil, v)
	panicIfErr(err)
}

func copyResp(f string, w http.ResponseWriter) {
	b, err := os.ReadFile(f)
	panicIfErr(err)
	_, err = io.Copy(w, bytes.NewReader(b))
	panicIfErr(err)
}

func TestClientConfig(t *testing.T) {
	fmt.Println("ClientConfig")

	cfg := &ClientConfig{}
	os.Setenv("CARTOGRAPHER_USER", "foo")
	os.Setenv("CARTOGRAPHER_KEY", "bar")
	cfg.MakeDefaults()
}

func TestClientWithViper(t *testing.T) {
	v := viper.New()
	v.Set("cartographer-user", "foo")
	v.Set("cartographer-key", "bar")
	got, err := NewClientWithViper(nil, v)
	require.NoError(t, err)
	require.NotNil(t, got)
}

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}
