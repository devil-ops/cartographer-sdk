package cartographer

import (
	"context"
	"fmt"
	"net/http"
)

const (
	devicePath = "/api/v1/devices"
)

type Device struct {
	BuildingID           *float64   `json:"building_id,omitempty"`
	BuildingLocationName *string    `json:"building_location_name,omitempty"`
	Function             string     `json:"function,omitempty"`
	ID                   float64    `json:"id,omitempty"`
	Ips                  []string   `json:"ips,omitempty"`
	LanID                *float64   `json:"lan_id,omitempty"`
	LastQueried          string     `json:"last_queried,omitempty"`
	MacAddresses         []string   `json:"mac_addresses,omitempty"`
	ManagementIp         *string    `json:"management_ip,omitempty"`
	Model                string     `json:"model,omitempty"`
	Name                 string     `json:"name,omitempty"`
	OneShotAvailable     bool       `json:"one_shot_available,omitempty"`
	OneShotEnabled       bool       `json:"one_shot_enabled,omitempty"`
	SecurityScansAllowed bool       `json:"security_scans_allowed,omitempty"`
	SerialNumber         string     `json:"serial_number,omitempty"`
	SoftwareVersion      string     `json:"software_version,omitempty"`
	Type                 string     `json:"type,omitempty"`
	URL                  string     `json:"url,omitempty"`
	VLANs                *[]float64 `json:"vlans,omitempty"`
}

type DeviceList []Device

type DeviceService interface {
	List(ctx *context.Context) (DeviceList, *Response, error)
	ListIPs(ctx *context.Context) ([]string, *Response, error)
	// Get(ctx *context.Context, name string) (*VRFWithSubnets, *Response, error)
}

func (svc *DeviceServiceOp) ListIPs(ctx *context.Context) ([]string, *Response, error) {
	returnItems := make([]string, 0)
	devices, _, _ := svc.List(ctx)
	for _, device := range devices {
		returnItems = append(returnItems, device.Ips...)
	}
	returnItems = Unique(returnItems)

	return returnItems, nil, nil
}

func (svc *DeviceServiceOp) List(ctx *context.Context) (DeviceList, *Response, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", svc.client.BaseURL, devicePath), nil)
	if err != nil {
		return nil, nil, err
	}
	var devices DeviceList
	_, err = svc.client.sendRequest(req, &devices)
	if err != nil {
		return nil, nil, err
	}
	return devices, nil, nil
}

type DeviceServiceOp struct {
	client *Client
}
