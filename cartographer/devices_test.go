package cartographer_test

import (
	"fmt"
	"io/ioutil"
	"testing"

	"github.com/jarcoal/httpmock"
	log "github.com/sirupsen/logrus"
	"gitlab.oit.duke.edu/devil-ops/cartographer-sdk/cartographer"
)

func TestDeviceListing(t *testing.T) {
	fmt.Println("TestDeviceListing")

	page, err := ioutil.ReadFile("./test_data/devices/list.json")
	if err != nil {
		log.Fatal(err)
	}
	httpmock.Activate()
	httpmock.RegisterResponder("GET", "/api/v1/devices",
		httpmock.NewStringResponder(200, string(page)))

	cfg := &cartographer.ClientConfig{}
	c := cartographer.NewClient(*cfg, nil)
	devices, _, err := c.Device.List(nil)
	if err != nil {
		t.Error(err)
	}

	httpmock.DeactivateAndReset()

	if len(devices) == 0 {
		t.Error("Device listing did not return any devices")
	}
}

func TestDeviceListingIPs(t *testing.T) {
	fmt.Println("TestDeviceListingIPs")

	page, err := ioutil.ReadFile("./test_data/devices/list.json")
	if err != nil {
		log.Fatal(err)
	}
	httpmock.Activate()
	httpmock.RegisterResponder("GET", "/api/v1/devices",
		httpmock.NewStringResponder(200, string(page)))

	cfg := &cartographer.ClientConfig{}
	c := cartographer.NewClient(*cfg, nil)
	data, _, err := c.Device.ListIPs(nil)
	if err != nil {
		t.Error(err)
	}

	httpmock.DeactivateAndReset()

	if len(data) == 0 {
		t.Error("Device IP listing did not return any device ips")
	}
}
