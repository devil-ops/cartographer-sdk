package cartographer

import (
	"encoding/binary"
	"fmt"
	"net"
)

// Taken from:
// https://stackoverflow.com/questions/41240761/check-if-ip-address-is-in-private-network-space
func IsPrivateIP(ip net.IP) bool {
	var privateIPBlocks []*net.IPNet
	for _, cidr := range []string{
		"127.0.0.0/8",    // IPv4 loopback
		"10.0.0.0/8",     // RFC1918
		"172.16.0.0/12",  // RFC1918
		"192.168.0.0/16", // RFC1918
		"169.254.0.0/16", // RFC3927 link-local
		"::1/128",        // IPv6 loopback
		"fe80::/10",      // IPv6 link-local
		"fc00::/7",       // IPv6 unique local addr
	} {
		_, block, err := net.ParseCIDR(cidr)
		if err != nil {
			panic(fmt.Errorf("parse error on %q: %v", cidr, err))
		}
		privateIPBlocks = append(privateIPBlocks, block)
	}

	if ip.IsLoopback() || ip.IsLinkLocalUnicast() || ip.IsLinkLocalMulticast() {
		return true
	}

	for _, block := range privateIPBlocks {
		if block.Contains(ip) {
			return true
		}
	}
	return false
}

func Unique(slice []string) []string {
	// create a map with all the values as key
	uniqMap := make(map[string]struct{})
	for _, v := range slice {
		uniqMap[v] = struct{}{}
	}

	// turn the map keys into a slice
	uniqSlice := make([]string, 0, len(uniqMap))
	for v := range uniqMap {
		uniqSlice = append(uniqSlice, v)
	}
	return uniqSlice
}

func BoundryIPStrings(network string) ([]string, error) {
	_, ipv4net, err := net.ParseCIDR(network)
	if err != nil {
		return nil, err
	}
	// Look up first, last, etc
	mask := binary.BigEndian.Uint32(ipv4net.Mask)
	start := binary.BigEndian.Uint32(ipv4net.IP)
	finish := (start & mask) | (mask ^ 0xffffffff)
	startIp := make(net.IP, 4)
	finishIp := make(net.IP, 4)
	binary.BigEndian.PutUint32(startIp, start)
	binary.BigEndian.PutUint32(finishIp, finish)
	return []string{startIp.String(), finishIp.String()}, nil
}

/*
func SliceContainsString(slice []string, item string) bool {
	set := make(map[string]struct{}, len(slice))
	for _, s := range slice {
		set[s] = struct{}{}
	}

	_, ok := set[item]
	return ok
}
*/

func SliceStringsEqual(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}
