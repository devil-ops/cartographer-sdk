package cartographer_test

import (
	"fmt"
	"net"
	"testing"

	"gitlab.oit.duke.edu/devil-ops/cartographer-sdk/cartographer"
)

func TestPrivateIPs(t *testing.T) {
	fmt.Println("TestPrivateIPs")
	tests := []struct {
		ip        string
		isPrivate bool
	}{
		{
			"127.0.0.1/32",
			true,
		},
		{
			"8.8.8.8/32",
			false,
		},
		{
			"10.30.40.50/32",
			true,
		},
		{
			"35.34.33.31/32",
			false,
		},
	}
	for _, test := range tests {
		ip, _, _ := net.ParseCIDR(test.ip)
		foundPrivate := cartographer.IsPrivateIP(ip)
		if foundPrivate != test.isPrivate {
			t.Errorf("Testing %s failed when evaluated private as %v when it should be %v", test.ip, foundPrivate, test.isPrivate)
		}
	}
}

func TestBoundryIPs(t *testing.T) {
	fmt.Println("TestBoundryIPs")
	tests := []struct {
		network   string
		boundries []string
	}{
		{
			"192.168.1.0/24",
			[]string{"192.168.1.0", "192.168.1.255"},
		},
	}
	for _, test := range tests {
		foundBoundries, _ := cartographer.BoundryIPStrings(test.network)
		if foundBoundries[0] != test.boundries[0] {
			t.Errorf("Wanted first boundry of %v but got %v", test.boundries[0], foundBoundries[0])
		}
		if foundBoundries[1] != test.boundries[1] {
			t.Errorf("Wanted last boundry of %v but got %v", test.boundries[1], foundBoundries[1])

		}
	}
}
