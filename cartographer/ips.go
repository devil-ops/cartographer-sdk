package cartographer

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/thoas/go-funk"
)

const (
	ipPath   = "/api/v1/ip"
	ipv4Path = "/api/v1/ipv4"
)

type LastSeenIP struct {
	IP       string
	LastSeen time.Time
}
type LastSeenIPList []LastSeenIP

type IP struct {
	OwningDeviceNames []string  `json:"owning_device_names,omitempty"`
	SubnetCIDR        string    `json:"subnet_cidr,omitempty"`
	SubnetName        string    `json:"subnet_name,omitempty"`
	VLANs             []float64 `json:"vlans,omitempty"`
	VRFs              []string  `json:"vrfs,omitempty"`
}

type IPV4Subnet struct {
	CIDR        string `json:"cidr,omitempty"`
	DukeregName string `json:"dukereg_name,omitempty"`
	Name        string `json:"name,omitempty"`
	VLANs       *[]struct {
		LAN       float64  `json:"lan,omitempty"`
		VLAN      float64  `json:"vlan,omitempty"`
		VLANNames []string `json:"vlan_names,omitempty"`
		VRF       string   `json:"vrf,omitempty"`
	} `json:"vlans,omitempty"`
	VRFs []string `json:"vrfs,omitempty"`
}

type IPService interface {
	LastSeen(ctx *context.Context, since *time.Time) ([]LastSeenIPList, *Response, error)
	Get(ctx *context.Context, ip string) (*IP, *Response, error)
	ListV4Subnets(ctx *context.Context) ([]IPV4Subnet, *Response, error)
	ListAllSubnetBoundries(ctx *context.Context) ([]string, *Response, error)
}

type IPServiceOp struct {
	client *Client
}

func (svc *IPServiceOp) Get(ctx *context.Context, ip string) (*IP, *Response, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s/%s", svc.client.BaseURL, ipPath, ip), nil)
	if err != nil {
		return nil, nil, err
	}
	var ipInfo IP
	_, err = svc.client.sendRequest(req, &ipInfo)
	if err != nil {
		return nil, nil, err
	}
	return &ipInfo, nil, nil
}
func (svc *IPServiceOp) ListAllSubnetBoundries(ctx *context.Context) ([]string, *Response, error) {
	subnets, _, err := svc.ListV4Subnets(ctx)
	var returnBoundries []string
	if err != nil {
		return nil, nil, err
	}
	for _, subnet := range subnets {
		boundries, err := BoundryIPStrings(subnet.CIDR)
		if err != nil {
			return nil, nil, err
		}
		returnBoundries = append(returnBoundries, boundries...)
	}
	r := funk.UniqString(returnBoundries)
	return r, nil, nil

}

func (svc *IPServiceOp) ListV4Subnets(ctx *context.Context) ([]IPV4Subnet, *Response, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s/subnets", svc.client.BaseURL, ipv4Path), nil)
	if err != nil {
		return nil, nil, err
	}
	var data []IPV4Subnet
	_, err = svc.client.sendRequest(req, &data)
	if err != nil {
		return nil, nil, err
	}
	return data, nil, nil
}

func (svc *IPServiceOp) LastSeen(ctx *context.Context, since *time.Time) ([]LastSeenIPList, *Response, error) {
	var from time.Time
	if since == nil {
		now := time.Now()
		from = now.AddDate(0, -1, 0)
	} else {
		from = *since
	}
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s/%s?since=%v", svc.client.BaseURL, ipPath, "last_seen", from.Unix()), nil)
	if err != nil {
		return nil, nil, err
	}
	var ips []LastSeenIPList
	_, err = svc.client.sendRequest(req, &ips)
	if err != nil {
		return nil, nil, err
	}
	return ips, nil, nil
}

func (r *LastSeenIPList) UnmarshalJSON(p []byte) error {
	var tmp []interface{}

	if err := json.Unmarshal(p, &tmp); err != nil {
		log.Println(err)
		return err
	}
	unix := tmp[1].(float64)
	ip := &LastSeenIP{
		IP:       fmt.Sprintf("%v", tmp[0].(string)),
		LastSeen: time.Unix(int64(unix), 0),
	}
	*r = append(*r, *ip)

	return nil

}
