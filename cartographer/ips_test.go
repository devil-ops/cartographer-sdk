package cartographer_test

import (
	"fmt"
	"io/ioutil"
	"testing"

	"github.com/jarcoal/httpmock"
	log "github.com/sirupsen/logrus"
	"gitlab.oit.duke.edu/devil-ops/cartographer-sdk/cartographer"
)

func TestIPLastSeen(t *testing.T) {
	fmt.Println("TestIPLastSeen")

	page, err := ioutil.ReadFile("./test_data/ips/last_seen.json")
	if err != nil {
		log.Fatal(err)
	}
	httpmock.Activate()
	httpmock.RegisterResponder("GET", "/api/v1/ip/last_seen",
		httpmock.NewStringResponder(200, string(page)))

	cfg := &cartographer.ClientConfig{}
	c := cartographer.NewClient(*cfg, nil)
	ips, _, err := c.IP.LastSeen(nil, nil)
	if err != nil {
		t.Error(err)
	}

	httpmock.DeactivateAndReset()

	if len(ips) == 0 {
		t.Error("IP listing did not return any last seen")
	}
}

func TestGetIP(t *testing.T) {
	fmt.Println("TestGetIP")

	page, err := ioutil.ReadFile("./test_data/ips/ip.json")
	if err != nil {
		log.Fatal(err)
	}
	httpmock.Activate()
	httpmock.RegisterResponder("GET", "/api/v1/ip/152.1.10.10",
		httpmock.NewStringResponder(200, string(page)))

	cfg := &cartographer.ClientConfig{}
	c := cartographer.NewClient(*cfg, nil)
	ip, _, err := c.IP.Get(nil, "152.1.10.10")
	if err != nil {
		t.Error(err)
	}

	if ip.SubnetCIDR != "152.1.10.0/22" {
		t.Error("Did not get expected subnet for GetIP")
	}

	httpmock.DeactivateAndReset()
}

func TestIPV4Subnets(t *testing.T) {
	fmt.Println("TestIPVRSubnets")

	page, err := ioutil.ReadFile("./test_data/ips/ipv4_subnets.json")
	if err != nil {
		log.Fatal(err)
	}
	httpmock.Activate()
	httpmock.RegisterResponder("GET", "/api/v1/ipv4/subnets",
		httpmock.NewStringResponder(200, string(page)))

	cfg := &cartographer.ClientConfig{}
	c := cartographer.NewClient(*cfg, nil)
	data, _, err := c.IP.ListV4Subnets(nil)
	if err != nil {
		t.Error(err)
	}

	httpmock.DeactivateAndReset()

	if len(data) == 0 {
		t.Error("IPV4 Subnet listing did not return any items")
	}
}

func TestIPV4SubnetBoundries(t *testing.T) {
	fmt.Println("TestIPVRSubnetBoundries")

	page, err := ioutil.ReadFile("./test_data/ips/ipv4_subnets.json")
	if err != nil {
		log.Fatal(err)
	}
	httpmock.Activate()
	httpmock.RegisterResponder("GET", "/api/v1/ipv4/subnets",
		httpmock.NewStringResponder(200, string(page)))

	cfg := &cartographer.ClientConfig{}
	c := cartographer.NewClient(*cfg, nil)
	data, _, err := c.IP.ListAllSubnetBoundries(nil)
	if err != nil {
		t.Error(err)
	}

	httpmock.DeactivateAndReset()

	wanted := []string{"10.1.1.0", "10.1.1.255", "10.100.0.0", "10.100.0.255"}
	if !cartographer.SliceStringsEqual(wanted, data) {
		t.Errorf("Boundries failed, wanted %v but got %v", wanted, data)
	}
}
