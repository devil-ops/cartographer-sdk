package cartographer

type ErrorResponse struct {
	Message string `json:"errors"`
}
