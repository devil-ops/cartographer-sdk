package cartographer

import (
	"context"
	"encoding/json"
	"fmt"
	"net"
	"net/http"

	"github.com/rs/zerolog/log"
)

const (
	vrfPath = "/api/v1/vrfs"
)

type VRF struct {
	Name     string
	IsLegacy bool
}
type VRFList []VRF

type VRFWithSubnets struct {
	Name    string
	Subnets []net.IPNet
}

// VRF Names have moved to a new name standard. We need to keep the old stuff
// around a bit, so adding this mapping and accompanying code
var legacyVRFMapping = map[string]string{
	"administrative": "users-3",
	"athletics":      "athletics-1",
	"campus":         "duke-1",
	"cloud":          "cloud-1",
	"console":        "console-1",
	"datacenter":     "dc-1",
	"dc-fw":          "dc-2",
	"dc-lb":          "dc-3",
	"dc-power":       "scada-2",
	"dco":            "devices-3",
	"intl":           "remote-1",
	"ips":            "netsec-1",
	"iptv":           "devices-2",
	"mgmt":           "mgmt-1",
	"pci":            "dnec-1",
	"psafety":        "safety-2",
	"pvt":            "devices-1",
	"research":       "users-2",
	"resnet":         "outlands-1",
	"scada-hv-1":     "scada-3",
	"siren":          "safety-1",
	"ssi-mgmt":       "mgmt-2",
	"uic":            "scada-1",
	"visitor":        "visitor-1",
	"voip":           "voice-1",
	"wireless":       "users-1",
}

// Return the new name from the legacy name
func GetVRFNameWithLegacy(name string) string {
	if v, ok := legacyVRFMapping[name]; ok {
		return v
	}
	return name
}

// Return the legacy name from the new name
func GetLegacyVRFName(name string) string {
	for k, v := range legacyVRFMapping {
		if v == name {
			return k
		}
	}
	return name
}

// Check if VRF name is legacy
func IsLegacyVRF(name string) bool {
	_, ok := legacyVRFMapping[name]
	return ok
}

// The VRF endpoint returns a list, so we need to map that in to structs
func (r *VRFList) UnmarshalJSON(p []byte) error {
	var tmp []interface{}
	if err := json.Unmarshal(p, &tmp); err != nil {
		return err
	}
	for _, item := range tmp {
		n := fmt.Sprintf("%v", item)
		vrf := &VRF{
			Name:     n,
			IsLegacy: IsLegacyVRF(n),
		}
		*r = append(*r, *vrf)

	}
	return nil
}

// Make the Subnet string real subnet objects when Unmarshalling
func (r *VRFWithSubnets) UnmarshalJSON(p []byte) error {
	var tmp map[string]interface{}
	if err := json.Unmarshal(p, &tmp); err != nil {
		return err
	}
	// Sweet lord, not sure why this works...TODO: Make cleaner?
	var subnetStringList []interface{} = tmp["subnets"].([]interface{})
	for _, subnetString := range subnetStringList {
		// Ignore ip return, we just want net and err
		_, net, err := net.ParseCIDR(fmt.Sprintf("%v", subnetString))
		if err != nil {
			return err
		}
		r.Subnets = append(r.Subnets, *net)

	}

	return nil
}

// VRFMapping represents a listing of all vrfs by name, along with their appropriate networks
type VRFMapping map[string][]net.IPNet

type VRFService interface {
	List(ctx *context.Context) (VRFList, *Response, error)
	Get(ctx *context.Context, name string) (*VRFWithSubnets, *Response, error)
	All(ctx context.Context) (*VRFMapping, *Response, error)
}

type VRFServiceOp struct {
	client *Client
}

func (svc *VRFServiceOp) All(ctx context.Context) (*VRFMapping, *Response, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", svc.client.BaseURL, vrfPath), nil)
	if err != nil {
		return nil, nil, err
	}
	var vrfs VRFList
	resp, err := svc.client.sendRequest(req, &vrfs)
	if err != nil {
		return nil, nil, err
	}
	ret := VRFMapping{}
	for _, vrf := range vrfs {
		ret[vrf.Name] = []net.IPNet{}
		vinfo, resp, err := svc.Get(&ctx, vrf.Name)
		if err != nil {
			log.Warn().Err(err).Str("vrf", vrf.Name).Send()
			return nil, resp, err
		}
		for _, net := range vinfo.Subnets {
			ret[vrf.Name] = append(ret[vrf.Name], net)
		}
	}
	return &ret, resp, nil
}

func (svc *VRFServiceOp) List(ctx *context.Context) (VRFList, *Response, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", svc.client.BaseURL, vrfPath), nil)
	if err != nil {
		return nil, nil, err
	}
	var vrfs VRFList
	_, err = svc.client.sendRequest(req, &vrfs)
	if err != nil {
		return nil, nil, err
	}
	return vrfs, nil, nil
}

func (svc *VRFServiceOp) Get(ctx *context.Context, name string) (*VRFWithSubnets, *Response, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s/%s", svc.client.BaseURL, vrfPath, name), nil)
	if err != nil {
		return nil, nil, err
	}
	var vrf *VRFWithSubnets
	_, err = svc.client.sendRequest(req, &vrf)
	if err != nil {
		return nil, nil, err
	}
	vrf.Name = name
	return vrf, nil, nil
}
