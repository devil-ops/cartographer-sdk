package cartographer

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestVRFListing(t *testing.T) {
	vrfs, _, err := c.VRF.List(nil)
	require.NoError(t, err)
	require.Greater(t, len(vrfs), 0)
}

func TestVRFGet(t *testing.T) {
	vrf, _, err := c.VRF.Get(nil, "vrf-foo")
	require.NoError(t, err)
	require.Greater(t, len(vrf.Subnets), 0)
}

func TestVRFAll(t *testing.T) {
	got, _, err := c.VRF.All(context.TODO())
	require.NoError(t, err)
	require.NotNil(t, got)
}
