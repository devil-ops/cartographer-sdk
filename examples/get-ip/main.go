package main

import (
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
	"gitlab.oit.duke.edu/devil-ops/cartographer-sdk/cartographer"
)

func main() {
	cfg := &cartographer.ClientConfig{}
	cfg.MakeDefaults()

	client := cartographer.NewClient(*cfg, nil)
	log.Println("Looking up ip")
	item, _, err := client.IP.Get(nil, os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%+v\n", item)
}
