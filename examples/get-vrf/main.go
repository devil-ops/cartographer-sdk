package main

import (
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
	"gitlab.oit.duke.edu/devil-ops/cartographer-sdk/cartographer"
)

func main() {
	cfg := &cartographer.ClientConfig{}
	cfg.MakeDefaults()

	client := cartographer.NewClient(*cfg, nil)
	log.Println("Looking up all addresses")
	vrf, _, err := client.VRF.Get(nil, os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	for _, subnet := range vrf.Subnets {
		fmt.Printf("Subnet: %v, Private: %v\n", subnet.String(), cartographer.IsPrivateIP(subnet.IP))
	}
}
