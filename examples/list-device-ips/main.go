package main

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.oit.duke.edu/devil-ops/cartographer-sdk/cartographer"
)

func main() {
	cfg := &cartographer.ClientConfig{}
	cfg.MakeDefaults()

	client := cartographer.NewClient(*cfg, nil)
	log.Println("Looking up all addresses")
	datas, _, err := client.Device.ListIPs(nil)
	if err != nil {
		log.Fatal(err)
	}
	for _, item := range datas {
		fmt.Println(item)
	}
}
