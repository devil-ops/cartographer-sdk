package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.oit.duke.edu/devil-ops/cartographer-sdk/cartographer"
)

func main() {
	cfg := &cartographer.ClientConfig{}
	cfg.MakeDefaults()

	client := cartographer.NewClient(*cfg, nil)
	log.Println("Looking up all addresses")
	datas, _, err := client.Device.List(nil)
	if err != nil {
		log.Fatal(err)
	}
	for _, item := range datas {
		log.Printf("%+v", item)
	}
}
