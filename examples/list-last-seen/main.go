package main

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.oit.duke.edu/devil-ops/cartographer-sdk/cartographer"
)

func main() {
	cfg := &cartographer.ClientConfig{}
	cfg.MakeDefaults()

	client := cartographer.NewClient(*cfg, nil)
	log.Println("Looking up last seen addresses")
	data, _, err := client.IP.LastSeen(nil, nil)
	if err != nil {
		log.Fatal(err)
	}
	for _, item := range data {
		fmt.Printf("%+v\n", item)
	}
}
