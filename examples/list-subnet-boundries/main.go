package main

import (
	"fmt"
	"sort"

	log "github.com/sirupsen/logrus"
	"gitlab.oit.duke.edu/devil-ops/cartographer-sdk/cartographer"
)

func main() {
	cfg := &cartographer.ClientConfig{}
	cfg.MakeDefaults()

	client := cartographer.NewClient(*cfg, nil)
	log.Println("Looking up IPV4 Subnet Boundries")
	data, _, err := client.IP.ListAllSubnetBoundries(nil)
	sort.Strings(data)
	if err != nil {
		log.Fatal(err)
	}
	for _, item := range data {
		fmt.Printf("%+v\n", item)
	}
}
