package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.oit.duke.edu/devil-ops/cartographer-sdk/cartographer"
)

func main() {
	cfg := &cartographer.ClientConfig{}
	cfg.MakeDefaults()

	log.Infof("%+v", cfg)

	client := cartographer.NewClient(*cfg, nil)
	log.Println("Looking up all addresses")
	vrfs, _, err := client.VRF.List(nil)
	if err != nil {
		log.Fatal(err)
	}
	for _, item := range vrfs {
		log.Printf("%+v", item)
	}
}
