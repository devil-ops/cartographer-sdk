package main

import (
	"encoding/csv"
	"os"

	log "github.com/sirupsen/logrus"
	"gitlab.oit.duke.edu/devil-ops/cartographer-sdk/cartographer"
)

func main() {
	if len(os.Args) != 2 {
		panic("Usage: script OUTPUT.csv")
	}
	outputFile := os.Args[1]

	cfg := &cartographer.ClientConfig{}
	cfg.MakeDefaults()

	client := cartographer.NewClient(*cfg, nil)

	// Set up CSV File
	file, err := os.Create(outputFile)
	CheckErr(err)
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	err = writer.Write([]string{"VRF", "Subnet", "Public/Private"})
	CheckErr(err)

	log.Println("Looking up all addresses")
	vrfs, _, err := client.VRF.List(nil)
	if err != nil {
		panic(err)
	}
	for _, vrf := range vrfs {
		fullVRF, _, err := client.VRF.Get(nil, vrf.Name)
		CheckErr(err)
		for _, subnet := range fullVRF.Subnets {
			var networkType string
			if cartographer.IsPrivateIP(subnet.IP) {
				networkType = "private"
			} else {
				networkType = "public"
			}

			row := []string{vrf.Name, subnet.String(), networkType}
			err := writer.Write(row)
			CheckErr(err)

		}
		// log.Println(vrf.Name, subnets)
	}
}

func CheckErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
